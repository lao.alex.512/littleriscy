/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file src/littleriscy_emulator/cpu_instruction_decode.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief RISC-V instruction decoder
 */

#include "cpu_instruction_decode.h"

#include <loguru.hpp>

/**
 * @brief Constructor
 */
cpu_instruction_decode::cpu_instruction_decode() {}

/**
 * @brief Decode an instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 */
cpu_state cpu_instruction_decode::decode_instruction(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_F(2, "decode_instruction: 0x%08X", instruction);

    // Detect instruction_type from opcode
    cpu_state decoded;

    switch (instruction & 0x7F) {
        case cpu_state::OPCODE_LOAD: {
            decoded = decode_load(instruction);
            break;
        }
        case cpu_state::OPCODE_LOAD_FP: {
            // Floating point not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_CUSTOM_0: {
            // Custom instructions not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_MISC_MEM: {
            // Fence instruction
            decoded.instruction_opcode = cpu_state::OPCODE_MISC_MEM;

            // TODO(lao.alex.512): Implement
            break;
        }
        case cpu_state::OPCODE_OP_IMM: {
            decoded = decode_op_imm(instruction);
            break;
        }
        case cpu_state::OPCODE_AUIPC: {
            decoded = decode_auipc(instruction);
            break;
        }
        case cpu_state::OPCODE_IMM_32: {
            // RV64I not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_STORE: {
            decoded = decode_store(instruction);
            break;
        }
        case cpu_state::OPCODE_STORE_FP: {
            // Floating point not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_CUSTOM_1: {
            // Custom instructions not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_AMO: {
            // Atomic instructions not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_OP: {
            decoded = decode_op(instruction);
            break;
        }
        case cpu_state::OPCODE_LUI: {
            decoded = decode_lui(instruction);
            break;
        }
        case cpu_state::OPCODE_OP32: {
            // RV64I not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_MADD: {
            // RV32F not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_MSUB: {
            // RV32F not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_NMSUB: {
            // RV32F not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_NMADD: {
            // RV32F not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_OP_FP: {
            // RV32F not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_RESERVED_0: {
            // Reserved for future use
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_CUSTOM_2_RV128: {
            // Custom instructions or RV128 not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_BRANCH: {
            decoded = decode_branch(instruction);
            break;
        }
        case cpu_state::OPCODE_JALR: {
            decoded = decode_jalr(instruction);
            break;
        }
        case cpu_state::OPCODE_RESERVED_1: {
            // Reserved for future use
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_JAL: {
            decoded = decode_jal(instruction);
            break;
        }
        case cpu_state::OPCODE_SYSTEM: {
            decoded = decode_system(instruction);
            break;
        }
        case cpu_state::OPCODE_RESERVED_2: {
            // Reserved for future use
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        case cpu_state::OPCODE_CUSTOM_3_RV128: {
            // Custom instructions or RV128 not implemented
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
        default: {
            decoded.instruction_opcode = cpu_state::OPCODE_UNKNOWN;
            break;
        }
    }

    if (decoded.instruction_opcode == cpu_state::OPCODE_UNKNOWN) {
        LOG_F(
            FATAL,
            "Unimplementated instruction");
    }

    return decoded;
}

/**
 * @brief Decode a load instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[14:12] = width[2:0]
 * instruction[19:15] = base[4:0]
 * instruction[31:20] = offset[11:0]
 */
cpu_state cpu_instruction_decode::decode_load(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_LOAD;

    decoded.funct3 = (instruction >> 12) & 0b111;
    decoded.left_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.left_operand_register_num = (instruction >> 15) & 0b11111;

    decoded.mem_op = cpu_state::MEM_OP_READ;

    decoded.dest_type = cpu_state::DEST_REGISTER;
    decoded.dest_register_num = (instruction >> 7) & 0b11111;

    decoded.right_operand_src_type = cpu_state::SRC_SIGNED_IMMEDIATE;
    decoded.signed_immediate = (int32_t) ((((instruction >> 31) & 0b1) ? 0xFFFFF000 : 0) | ((instruction >> 20) & 0xFFF));

    LOG_F(
        3,
        "SRC1 REG: %d, IMM: %d, DEST REG: %d",
        static_cast<int>(decoded.left_operand_register_num),
        static_cast<int>(decoded.signed_immediate),
        static_cast<int>(decoded.dest_register_num));

    return decoded;
}

/**
 * @brief Decode an operate instruction with an immediate value
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = src[4:0]
 * instruction[31:20] = I-immediate[11:0] or U-immediate[11:0]
 */
cpu_state cpu_instruction_decode::decode_op_imm(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_OP_IMM;

    decoded.funct3 = (instruction >> 12) & 0b111;

    decoded.left_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.left_operand_register_num = (instruction >> 15) & 0b11111;

    decoded.dest_type = cpu_state::DEST_REGISTER;
    decoded.dest_register_num = (instruction >> 7) & 0b11111;

    decoded.right_operand_src_type = cpu_state::SRC_SIGNED_IMMEDIATE;

    if (decoded.funct3 == cpu_state::FUNCT3_OP_IMM_SLTIU) {
        decoded.right_operand_src_type = cpu_state::SRC_UNSIGNED_IMMEDIATE;
    }

    decoded.signed_immediate = (int32_t) ((((instruction >> 31) & 0b1) ? 0xFFFFF000 : 0) | ((instruction >> 20) & 0xFFF));
    // SLTIU still sign extends the unsigned value before doing the comparison as an unsigned
    decoded.unsigned_immediate = (uint32_t) ((((instruction >> 31) & 0b1) ? 0xFFFFF000 : 0) | ((instruction >> 20) & 0xFFF));

    decoded.mem_op = cpu_state::MEM_OP_NONE;

    LOG_F(
        3,
        "SRC1 REG: %d, IMM: %d, DEST REG: %d",
        static_cast<int>(decoded.left_operand_register_num),
        static_cast<int>(decoded.signed_immediate),
        static_cast<int>(decoded.dest_register_num));

    return decoded;
}

/**
 * @brief Decode an add upper immediate to program counter instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[31:12] = U-immediate[31:12]
 */
cpu_state cpu_instruction_decode::decode_auipc(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_AUIPC;

    decoded.left_operand_src_type = cpu_state::SRC_SIGNED_IMMEDIATE;
    decoded.signed_immediate = (int32_t) (instruction & 0xFFFFF000);

    decoded.right_operand_src_type = cpu_state::SRC_PROGRAM_COUNTER;

    decoded.dest_type = cpu_state::DEST_REGISTER;
    decoded.dest_register_num = (instruction >> 7) & 0b11111;

    decoded.mem_op = cpu_state::MEM_OP_NONE;

    LOG_F(
        3,
        "IMM: %d, DEST REG: %d",
        static_cast<int>(decoded.signed_immediate),
        static_cast<int>(decoded.dest_register_num));

    return decoded;
}

/**
 * @brief Decode a store instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = offset[4:0]
 * instruction[14:12] = width[2:0]
 * instruction[19:15] = base[4:0]
 * instruction[24:20] = src[4:0]
 * instruction[31:25] = offset[11:5]
 */
cpu_state cpu_instruction_decode::decode_store(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_STORE;

    decoded.funct3 = (instruction >> 12) & 0b111;

    decoded.left_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.left_operand_register_num = (instruction >> 15) & 0b11111;

    decoded.right_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.right_operand_register_num = (instruction >> 20) & 0b11111;

    decoded.mem_op = cpu_state::MEM_OP_WRITE;

    decoded.signed_immediate = static_cast<int>(
        (((instruction >> 31) & 0b1) ? 0xFFFFF000 : 0) |
        (((instruction >> 25) & 0b1111111) << 5) | ((instruction >> 7) & 0b11111));

    LOG_F(
        3,
        "SRC1 REG (BASE): %d, SRC2 REG (SRC): %d, IMM (OFFSET): %d",
        static_cast<int>(decoded.left_operand_register_num),
        static_cast<int>(decoded.right_operand_register_num),
        static_cast<int>(decoded.signed_immediate));

    return decoded;
}

/**
 * @brief Decode an operate instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = src1[4:0]
 * instruction[24:20] = src2[4:0]
 * instruction[31:25] = funct7[6:0]
 */
cpu_state cpu_instruction_decode::decode_op(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_OP;

    decoded.funct3 = (instruction >> 12) & 0b111;
    decoded.funct7 = (instruction >> 25) & 0b1111111;

    decoded.left_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.left_operand_register_num = (instruction >> 15) & 0b11111;

    decoded.right_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.right_operand_register_num = (instruction >> 20) & 0b11111;

    decoded.dest_type = cpu_state::DEST_REGISTER;
    decoded.dest_register_num = (instruction >> 7) & 0b11111;

    decoded.mem_op = cpu_state::MEM_OP_NONE;

    LOG_F(
        3,
        "SRC1 REG: %d, SRC2 REG: %d, DEST REG: %d",
        static_cast<int>(decoded.left_operand_register_num),
        static_cast<int>(decoded.right_operand_register_num),
        static_cast<int>(decoded.dest_register_num));

    return decoded;
}

/**
 * @brief Decode a load upper immediate instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[31:12] = U-immediate[31:12]
 */
cpu_state cpu_instruction_decode::decode_lui(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_LUI;

    decoded.left_operand_src_type = cpu_state::SRC_UNSIGNED_IMMEDIATE;
    decoded.unsigned_immediate = (uint32_t) (instruction & 0xFFFFF000);

    decoded.dest_type = cpu_state::DEST_REGISTER;
    decoded.dest_register_num = (instruction >> 7) & 0b11111;

    decoded.mem_op = cpu_state::MEM_OP_NONE;

    LOG_F(
        3,
        "SRC1 REG = %d, IMM = %d, DEST REG = %d",
        static_cast<int>(decoded.left_operand_register_num),
        static_cast<int>(decoded.unsigned_immediate),
        static_cast<int>(decoded.dest_register_num));

    return decoded;
}

/**
 * @brief Decode a branch instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[7]     = offset[11]
 * instruction[11:8]  = offset[4:1]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = src1[4:0]
 * instruction[24:20] = src2[4:0]
 * instruction[30:25] = offset[10:5]
 * instruction[31]    = offset[12]
 */
cpu_state cpu_instruction_decode::decode_branch(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_BRANCH;

    decoded.funct3 = (instruction >> 12) & 0b111;

    decoded.left_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.left_operand_register_num = (instruction >> 15) & 0b11111;

    decoded.right_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.right_operand_register_num = (instruction >> 20) & 0b11111;

    decoded.signed_immediate = 0;
    decoded.signed_immediate |= ((instruction >> 8) & 0b1111) << 1;
    decoded.signed_immediate |= ((instruction >> 25) & 0b111111) << 5;
    decoded.signed_immediate |= ((instruction >> 7) & 0b1) << 11;
    decoded.signed_immediate |= ((instruction >> 31) & 0b1) ? 0xFFFFF000 : 0;

    decoded.mem_op = cpu_state::MEM_OP_NONE;

    LOG_F(
        3,
        "SRC1 REG: %d, SRC2 REG: %d, IMM (OFFSET): %d",
        static_cast<int>(decoded.left_operand_register_num),
        static_cast<int>(decoded.right_operand_register_num),
        static_cast<int>(decoded.signed_immediate));

    return decoded;
}

/**
 * @brief Decode a jump and link register instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = base[4:0]
 * instruction[31:20] = offset[11:0]
 */
cpu_state cpu_instruction_decode::decode_jalr(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_JALR;

    decoded.funct3 = (instruction >> 12) & 0b111;

    decoded.left_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.left_operand_register_num = (instruction >> 15) & 0b11111;

    decoded.dest_type = cpu_state::DEST_REGISTER;
    decoded.dest_register_num = (instruction >> 7) & 0b11111;

    decoded.right_operand_src_type = cpu_state::SRC_SIGNED_IMMEDIATE;
    decoded.signed_immediate = (int32_t) ((((instruction >> 31) & 0b1) ? 0xFFFFF000 : 0) | ((instruction >> 20) & 0xFFF));

    decoded.mem_op = cpu_state::MEM_OP_NONE;

    LOG_F(
        3,
        "SRC1 REG: %d, IMM (OFFSET): %d, DEST REG = %d",
        static_cast<int>(decoded.left_operand_register_num),
        static_cast<int>(decoded.signed_immediate),
        static_cast<int>(decoded.dest_register_num));

    return decoded;
}

/**
 * @brief Decode a jump and link instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[19:12] = offset[19:12]
 * instruction[20]    = offset[11]
 * instruction[30:21] = offset[10:1]
 * instruction[31]    = offset[20]
 */
cpu_state cpu_instruction_decode::decode_jal(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_JAL;

    decoded.dest_type = cpu_state::DEST_REGISTER;
    decoded.dest_register_num = (instruction >> 7) & 0b11111;

    decoded.signed_immediate = 0;
    decoded.signed_immediate |= ((instruction >> 21) & 0b1111111111) << 1;
    decoded.signed_immediate |= ((instruction >> 20) & 0b1) << 11;
    decoded.signed_immediate |= ((instruction >> 12) & 0b11111111) << 12;
    decoded.signed_immediate |= ((instruction >> 31) & 0b1) ? 0xFFF00000 : 0;

    LOG_F(
        3,
        "IMM (OFFSET): %d, DEST REG: %d",
        static_cast<int>(decoded.signed_immediate),
        static_cast<int>(decoded.dest_register_num));

    return decoded;
}

/**
 * @brief Decode a system instruction
 *
 * @returns cpu_state that contains the pipeline state after decoding
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = rd[4:0]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = rs1[4:0]
 * instruction[31:20] = funct12[11:0]
 */
cpu_state cpu_instruction_decode::decode_system(
    uint32_t instruction  //!< Raw Instruction
) {
    LOG_SCOPE_FUNCTION(3);

    cpu_state decoded;

    decoded.instruction_opcode = cpu_state::OPCODE_SYSTEM;

    decoded.funct3 = (instruction >> 12) & 0b111;
    decoded.funct12 = instruction >> 20;

    decoded.left_operand_src_type = cpu_state::SRC_REGISTER;
    decoded.left_operand_register_num = (instruction >> 15) & 0b11111;

    decoded.dest_type = cpu_state::DEST_REGISTER;
    decoded.dest_register_num = (instruction >> 7) & 0b11111;

    LOG_F(
        3,
        "SRC1 REG: %d, DEST REG: %d",
        static_cast<int>(decoded.left_operand_register_num),
        static_cast<int>(decoded.dest_register_num));

    return decoded;
}
