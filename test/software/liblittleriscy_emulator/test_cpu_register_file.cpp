/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file test/software/liblittleriscy_emulator/test_cpu_register_file.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2021
 *
 * @brief Test the CPU Register File
 */

#include "gtest/gtest.h"

#include "cpu_register_file.h"

TEST(test_cpu_register_file, unwritable_register_zero) {
    cpu_register_file test_register_file;

    test_register_file.write_reg(0, 0xAAAA);
    EXPECT_EQ(test_register_file.read_reg(0), 0);
}

TEST(test_cpu_register_file, reset_values) {
    cpu_register_file test_register_file;

    for (int i = 0; i < 32; i++) {
        EXPECT_EQ(test_register_file.read_reg(i), 0);
    }
}

TEST(test_cpu_register_file, read_write) {
    cpu_register_file test_register_file;

    for (int i = 0; i < 32; i++) {
        test_register_file.write_reg(i, i);
    }

    for (int i = 0; i < 32; i++) {
        EXPECT_EQ(test_register_file.read_reg(i), i);
    }
}
