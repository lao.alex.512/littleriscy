/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file include/cpu_state.h
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief Class for holding CPU state between stages
 */

#ifndef SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_STATE_H_
#define SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_STATE_H_

#include <cstdint>

class cpu_state {
 public:
    cpu_state();

    /*
     * instruction to opcode mapping, TABLE 25.1
     */
    typedef enum {
        OPCODE_LOAD           = 0b0000011,
        OPCODE_LOAD_FP        = 0b0000111,
        OPCODE_CUSTOM_0       = 0b0001011,
        OPCODE_MISC_MEM       = 0b0001111,
        OPCODE_OP_IMM         = 0b0010011,
        OPCODE_AUIPC          = 0b0010111,
        OPCODE_IMM_32         = 0b0011011,
        OPCODE_STORE          = 0b0100011,
        OPCODE_STORE_FP       = 0b0100111,
        OPCODE_CUSTOM_1       = 0b0101011,
        OPCODE_AMO            = 0b0101111,
        OPCODE_OP             = 0b0110011,
        OPCODE_LUI            = 0b0110111,
        OPCODE_OP32           = 0b0111011,
        OPCODE_MADD           = 0b1000011,
        OPCODE_MSUB           = 0b1000111,
        OPCODE_NMSUB          = 0b1001011,
        OPCODE_NMADD          = 0b1001111,
        OPCODE_OP_FP          = 0b1010011,
        OPCODE_RESERVED_0     = 0b1010111,
        OPCODE_CUSTOM_2_RV128 = 0b1011011,
        OPCODE_BRANCH         = 0b1100011,
        OPCODE_JALR           = 0b1100111,
        OPCODE_RESERVED_1     = 0b1101011,
        OPCODE_JAL            = 0b1101111,
        OPCODE_SYSTEM         = 0b1110011,
        OPCODE_RESERVED_2     = 0b1110111,
        OPCODE_CUSTOM_3_RV128 = 0b1111011,
        OPCODE_UNKNOWN
    } instruction_opcode_t;

    typedef enum {
        FUNCT3_LOAD_LB = 0b000,   // Load Byte (8 bit) - Sign Extend
        FUNCT3_LOAD_LH = 0b001,   // Load Half (16 bit) - Sign Extend
        FUNCT3_LOAD_LW = 0b010,   // Load Word (32bit)
        FUNCT3_LOAD_LBU = 0b100,  // Load Byte Unsigned (8 bit) - Zero Extend
        FUNCT3_LOAD_LHU = 0b101,  // Load Half Unsigned (16 bit) - Zero Extend
        FUNCT3_LOAD_UNKNOWN
    } funct3_load_t;

    typedef enum {
        FUNCT3_STORE_SB = 0b000,  // Store Byte (8 bit)
        FUNCT3_STORE_SH = 0b001,  // Store Half (16 bit)
        FUNCT3_STORE_SW = 0b010,  // Store Word (32bit)
        FUNCT3_STORE_UNKNOWN
    } funct3_store_t;

    typedef enum {
        FUNCT3_OP_IMM_ADDI = 0b000,
        FUNCT3_OP_IMM_SLTI = 0b010,
        FUNCT3_OP_IMM_SLTIU = 0b011,
        FUNCT3_OP_IMM_XORI = 0b100,
        FUNCT3_OP_IMM_ORI = 0b110,
        FUNCT3_OP_IMM_ANDI = 0b111,
        FUNCT3_OP_IMM_SLLI = 0b001,
        FUNCT3_OP_IMM_SRLI_SRAI = 0b101,
        FUNCT3_OP_IMM_UNKNOWN
    } funct3_op_imm_t;

    typedef enum {
        FUNCT3_OP_ADD_SUB = 0b000,
        FUNCT3_OP_SLL = 0b001,
        FUNCT3_OP_SLT = 0b010,
        FUNCT3_OP_SLTU = 0b011,
        FUNCT3_OP_XOR = 0b100,
        FUNCT3_OP_SRL_SRA = 0b101,
        FUNCT3_OP_OR = 0b110,
        FUNCT3_OP_AND = 0b111,
        FUNCT3_OP_UNKNOWN
    } funct3_op_t;

    typedef enum {
        FUNCT3_BRANCH_BEQ = 0b000,
        FUNCT3_BRANCH_BNE = 0b001,
        FUNCT3_BRANCH_BLT = 0b100,
        FUNCT3_BRANCH_BGE = 0b101,
        FUNCT3_BRANCH_BLTU = 0b110,
        FUNCT3_BRANCH_BGEU = 0b111,
        FUNCT3_BRANCH_UNKOWN
    } funct3_branch_t;

    typedef enum {
        SRC_PROGRAM_COUNTER,
        SRC_UNSIGNED_IMMEDIATE,
        SRC_SIGNED_IMMEDIATE,
        SRC_REGISTER,
        SRC_NONE
    } operand_src_type_t;

    typedef enum {
        MEM_OP_WRITE,
        MEM_OP_READ,
        MEM_OP_NONE
    } mem_op_type_t;

    typedef enum {
        DEST_REGISTER,
        DEST_NONE
    } dst_type_t;

    // Written by instruction decoder
    instruction_opcode_t instruction_opcode;
    uint32_t unsigned_immediate = 0;
    int32_t signed_immediate = 0;
    operand_src_type_t left_operand_src_type = SRC_NONE;
    uint8_t left_operand_register_num = 0;  // rs1
    operand_src_type_t right_operand_src_type = SRC_NONE;
    uint8_t right_operand_register_num = 0;  // rs2
    mem_op_type_t mem_op = MEM_OP_NONE;
    dst_type_t dest_type = DEST_NONE;
    uint8_t dest_register_num = 0;
    uint8_t funct3 = 0;
    uint8_t funct7 = 0;
    uint16_t funct12 = 0;

    // Written by instruction executor
    uint32_t left_operand_register_value = 0;
    uint32_t right_operand_register_value = 0;
    int32_t result = 0;
    int32_t memory_addr = 0;
    uint32_t jump_addr = 0;
};

#endif  // SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_STATE_H_
