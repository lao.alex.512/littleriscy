/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file src/littleriscy_emulator/cpu_register_file.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief RISC-V register file
 */

#include "cpu_register_file.h"

#include <loguru.hpp>

/**
 * @brief Constructor
 */
cpu_register_file::cpu_register_file() {
    for (int i = 0; i < NUM_REGS; i++) {
        reg_store[i] = 0;
    }
}

/**
 * @brief Write to a CPU register
 */
void cpu_register_file::write_reg(
    uint32_t reg_num,  //!< Register number
    uint32_t data      //!< Data to write
) {
    assert(reg_num < NUM_REGS);

    LOG_SCOPE_FUNCTION(3);

    if (reg_num != 0) {
        LOG_F(3, "WRITE REG %d = %d", reg_num, data);
        reg_store[reg_num] = data;
    } else {
        LOG_F(3, "IGNORING WRITE TO REG %d = %d", reg_num, data);
    }
}

/**
 * @brief Read from a CPU register
 *
 * @returns Value read from the selected register
 */
uint32_t cpu_register_file::read_reg(
    uint32_t reg_num  //!< Register number
) {
    assert(reg_num < NUM_REGS);

    LOG_SCOPE_FUNCTION(3);

    LOG_F(3, "READ REG %d = %d", reg_num, reg_store[reg_num]);

    return reg_store[reg_num];
}
