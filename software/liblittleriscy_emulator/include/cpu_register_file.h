/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file include/cpu_register_file.h
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief Header for cpu_register_file.cpp
 */

#ifndef SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_REGISTER_FILE_H_
#define SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_REGISTER_FILE_H_

#define NUM_REGS 32

#include <cassert>
#include <cstdint>

class cpu_register_file {
 public:
    cpu_register_file();
    uint32_t read_reg(uint32_t reg_num);
    void write_reg(uint32_t reg_num, uint32_t data);

    uint32_t program_counter = 0;
 private:
    uint32_t reg_store[NUM_REGS];
};

#endif  // SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_REGISTER_FILE_H_
