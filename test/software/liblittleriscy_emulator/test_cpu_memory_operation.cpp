/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file test/software/liblittleriscy_emulator/test_cpu_memory_operation.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2021
 *
 * @brief Test the CPU Memory Operation executor
 */

#include <cstdint>
#include <random>

#include "gtest/gtest.h"

#include "cpu_memory_operation.h"
#include "cpu_state.h"
#include "ram_peripheral.h"

/**
 * @brief Test execute_memory_operation for full word write
 */
TEST(test_cpu_memory_operation, test_mem_op_write) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_test_value(0, 4294967295);
    std::uniform_int_distribution<unsigned int> uniform_test_addr(0, 9);

    ram_peripheral test_ram(10);
    cpu_memory_operation test_cpu_mem_op(&test_ram);
    cpu_state test_state;

    uint32_t test_value = 0;
    uint32_t test_addr = 0;

    test_state.mem_op = cpu_state::MEM_OP_WRITE;

    for (int i = 0; i < 1000; i++) {
        test_value = uniform_test_value(rand_engine);
        test_addr = 4*uniform_test_addr(rand_engine);

        test_state.result = test_value;
        test_state.memory_addr = test_addr;

        test_cpu_mem_op.execute_memory_operation(&test_state);

        EXPECT_EQ(test_ram.read(test_addr/4), test_value);
    }
}

/**
 * @brief Test execute_memory_operation for load byte with sign extension
 */
TEST(test_cpu_memory_operation, test_mem_op_read_load_byte) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_test_value_full_range(0, 4294967295);
    std::uniform_int_distribution<unsigned int> uniform_test_value_byte(0, 255);
    std::uniform_int_distribution<unsigned int> uniform_test_addr(0, 9);

    ram_peripheral test_ram(10);
    cpu_memory_operation test_cpu_mem_op(&test_ram);
    cpu_state test_state;

    int32_t test_value = 0;
    uint32_t test_addr = 0;

    test_state.mem_op = cpu_state::MEM_OP_READ;
    test_state.funct3 = cpu_state::FUNCT3_LOAD_LB;

    for (int i = 0; i < 1000; i++) {
        test_value = uniform_test_value_full_range(rand_engine);
        test_addr = 4*uniform_test_addr(rand_engine);

        test_ram.write(test_addr/4, test_value & 0xFF);

        test_state.memory_addr = test_addr;

        test_cpu_mem_op.execute_memory_operation(&test_state);

        EXPECT_EQ(test_state.result, (int8_t) (test_value & 0xFF));

        if (((int8_t) (test_value & 0xFF)) >= 0) {
            EXPECT_EQ(test_state.result >> 8, 0);
        } else {
            EXPECT_EQ(test_state.result >> 8, -1);
        }
    }

    for (int i = 0; i < 1000; i++) {
        test_value = uniform_test_value_byte(rand_engine);
        test_addr = 4*uniform_test_addr(rand_engine);

        test_ram.write(test_addr/4, test_value & 0xFF);

        test_state.memory_addr = test_addr;

        test_cpu_mem_op.execute_memory_operation(&test_state);

        EXPECT_EQ(test_state.result, (int8_t) (test_value & 0xFF));

        if (((int8_t) (test_value & 0xFF)) >= 0) {
            EXPECT_EQ(test_state.result >> 8, 0);
        } else {
            EXPECT_EQ(test_state.result >> 8, -1);
        }
    }
}

/**
 * @brief Test execute_memory_operation for load half word with sign extension
 */
TEST(test_cpu_memory_operation, test_mem_op_read_load_half_word) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_test_value_full_range(0, 4294967295);
    std::uniform_int_distribution<unsigned int> uniform_test_value_half_word(0, 65535);
    std::uniform_int_distribution<unsigned int> uniform_test_addr(0, 9);

    ram_peripheral test_ram(10);
    cpu_memory_operation test_cpu_mem_op(&test_ram);
    cpu_state test_state;

    int32_t test_value = 0;
    uint32_t test_addr = 0;

    test_state.mem_op = cpu_state::MEM_OP_READ;
    test_state.funct3 = cpu_state::FUNCT3_LOAD_LH;

    for (int i = 0; i < 1000; i++) {
        test_value = uniform_test_value_full_range(rand_engine);
        test_addr = 4*uniform_test_addr(rand_engine);

        test_ram.write(test_addr/4, test_value);

        test_state.memory_addr = test_addr;

        test_cpu_mem_op.execute_memory_operation(&test_state);

        EXPECT_EQ(test_state.result, (int16_t) (test_value & 0xFFFF));

        if (((int16_t) (test_value & 0xFFFF)) >= 0) {
            EXPECT_EQ(test_state.result >> 16, 0);
        } else {
            EXPECT_EQ(test_state.result >> 16, -1);
        }
    }

    for (int i = 0; i < 1000; i++) {
        test_value = uniform_test_value_half_word(rand_engine);
        test_addr = 4*uniform_test_addr(rand_engine);

        test_ram.write(test_addr/4, test_value);

        test_state.memory_addr = test_addr;

        test_cpu_mem_op.execute_memory_operation(&test_state);

        EXPECT_EQ(test_state.result, (int16_t) (test_value & 0xFFFF));

        if (((int16_t) (test_value & 0xFFFF)) >= 0) {
            EXPECT_EQ(test_state.result >> 16, 0);
        } else {
            EXPECT_EQ(test_state.result >> 16, -1);
        }
    }
}

/**
 * @brief Test execute_memory_operation for load word with sign extension
 */
TEST(test_cpu_memory_operation, test_mem_op_read_load_word) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<int> uniform_test_value(-2147483648, 2147483647);
    std::uniform_int_distribution<unsigned int> uniform_test_addr(0, 9);

    ram_peripheral test_ram(10);
    cpu_memory_operation test_cpu_mem_op(&test_ram);
    cpu_state test_state;

    int32_t test_value = 0;
    uint32_t test_addr = 0;

    test_state.mem_op = cpu_state::MEM_OP_READ;
    test_state.funct3 = cpu_state::FUNCT3_LOAD_LW;

    for (int i = 0; i < 1000; i++) {
        test_value = uniform_test_value(rand_engine);
        test_addr = 4*uniform_test_addr(rand_engine);

        test_ram.write(test_addr/4, test_value);

        test_state.memory_addr = test_addr;

        test_cpu_mem_op.execute_memory_operation(&test_state);

        EXPECT_EQ(test_state.result, test_value);
    }
}

/**
 * @brief Test execute_memory_operation for load byte unsigned
 */
TEST(test_cpu_memory_operation, test_mem_op_read_load_byte_unsigned) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_test_value(0, 4294967295);
    std::uniform_int_distribution<unsigned int> uniform_test_addr(0, 9);

    ram_peripheral test_ram(10);
    cpu_memory_operation test_cpu_mem_op(&test_ram);
    cpu_state test_state;

    uint32_t test_value = 0;
    uint32_t test_addr = 0;

    test_state.mem_op = cpu_state::MEM_OP_READ;
    test_state.funct3 = cpu_state::FUNCT3_LOAD_LBU;

    for (int i = 0; i < 1000; i++) {
        test_value = uniform_test_value(rand_engine);
        test_addr = 4*uniform_test_addr(rand_engine);

        test_ram.write(test_addr/4, test_value);

        test_state.memory_addr = test_addr;

        test_cpu_mem_op.execute_memory_operation(&test_state);

        EXPECT_EQ(test_state.result, test_value & 0xFF);
    }
}

/**
 * @brief Test execute_memory_operation for load half word unsigned
 */
TEST(test_cpu_memory_operation, test_mem_op_read_load_half_word_unsigned) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_test_value(0, 4294967295);
    std::uniform_int_distribution<unsigned int> uniform_test_addr(0, 9);

    ram_peripheral test_ram(10);
    cpu_memory_operation test_cpu_mem_op(&test_ram);
    cpu_state test_state;

    uint32_t test_value = 0;
    uint32_t test_addr = 0;

    test_state.mem_op = cpu_state::MEM_OP_READ;
    test_state.funct3 = cpu_state::FUNCT3_LOAD_LHU;

    for (int i = 0; i < 1000; i++) {
        test_value = uniform_test_value(rand_engine);
        test_addr = 4*uniform_test_addr(rand_engine);

        test_ram.write(test_addr/4, test_value);

        test_state.memory_addr = test_addr;

        test_cpu_mem_op.execute_memory_operation(&test_state);

        EXPECT_EQ(test_state.result, test_value & 0xFFFF);
    }
}
