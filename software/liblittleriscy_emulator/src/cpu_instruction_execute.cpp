/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file src/littleriscy_emulator/cpu_instruction_execute.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief RISC-V instruction execute stage
 */

#include "cpu_instruction_execute.h"

#include <loguru.hpp>

/**
 * @brief Constructor
 */
cpu_instruction_execute::cpu_instruction_execute(
    cpu_register_file *reg_file
): reg_file(reg_file) {}

/**
 * @brief Execute the instruction that has been decoded into a cpu_state
 */
void cpu_instruction_execute::execute_instruction(
    cpu_state *state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(2);

    state->left_operand_register_value = this->reg_file->read_reg(state->left_operand_register_num);
    state->right_operand_register_value = this->reg_file->read_reg(state->right_operand_register_num);

    switch (state->instruction_opcode) {
        case cpu_state::OPCODE_LOAD: {
            execute_load(state);
            break;
        }
        case cpu_state::OPCODE_MISC_MEM: {
            // TODO(lao.alex.512): Implement
            break;
        }
        case cpu_state::OPCODE_OP_IMM: {
            execute_op_imm(state);
            break;
        }
        case cpu_state::OPCODE_AUIPC: {
            execute_auipc(state);
            break;
        }
        case cpu_state::OPCODE_STORE: {
            execute_store(state);
            break;
        }
        case cpu_state::OPCODE_OP: {
            execute_op(state);
            break;
        }
        case cpu_state::OPCODE_LUI: {
            execute_lui(state);
            break;
        }
        case cpu_state::OPCODE_BRANCH: {
            execute_branch(state);
            break;
        }
        case cpu_state::OPCODE_JAL: {
            execute_jal(state);
            break;
        }
        case cpu_state::OPCODE_JALR: {
            execute_jalr(state);
            break;
        }
        case cpu_state::OPCODE_SYSTEM: {
            // TODO(lao.alex.512): Implement
            break;
        }
        default: {
            LOG_F(
                FATAL,
                "Unimplementated instruction");
            break;
        }
    }
}

/**
 * @brief Execute a load instruction
 */
void cpu_instruction_execute::execute_load(
    cpu_state* state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    int32_t base = state->left_operand_register_value;
    int32_t offset = state->signed_immediate;
    state->memory_addr = base + offset;

    LOG_F(
        3,
        "REG: %d = DATA at MEM ADDR: %d",
        static_cast<uint32_t>(state->dest_register_num),
        state->memory_addr);
}

/**
 * @brief Execute a operate on immediate value instruction
 */
void cpu_instruction_execute::execute_op_imm(
    cpu_state* state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    int32_t left_operand = state->left_operand_register_value;
    int32_t right_operand_signed = state->signed_immediate;
    int32_t right_operand_unsigned = state->unsigned_immediate;

    switch (state->funct3) {
        case cpu_state::FUNCT3_OP_IMM_ADDI: {
            LOG_F(
                3,
                "OP IMM ADDI");

            state->result = left_operand + right_operand_signed;

            LOG_F(
                3,
                "%d + %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand_signed),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_IMM_SLTI: {
            LOG_F(
                3,
                "OP IMM SLTI");

            state->result = left_operand < right_operand_signed;

            LOG_F(
                3,
                "%d < %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand_signed),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_IMM_SLTIU: {
            LOG_F(
                3,
                "OP IMM SLTIU");

            state->result = ((uint32_t) left_operand) < right_operand_unsigned;

            LOG_F(
                3,
                "%d <U %d = %d",
                static_cast<uint32_t>(left_operand),
                static_cast<uint32_t>(right_operand_unsigned),
                static_cast<uint32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_IMM_XORI: {
            LOG_F(
                3,
                "OP IMM XORI");

            state->result = left_operand ^ right_operand_signed;

            LOG_F(
                3,
                "%d XOR %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand_signed),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_IMM_ORI: {
            LOG_F(
                3,
                "OP IMM ORI");

            state->result = left_operand | right_operand_signed;

            LOG_F(
                3,
                "%d OR %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand_signed),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_IMM_ANDI: {
            LOG_F(
                3,
                "OP IMM ANDI");

            state->result = left_operand & right_operand_signed;

            LOG_F(
                3,
                "%d AND %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand_signed),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_IMM_SLLI: {
            LOG_F(
                3,
                "OP IMM SLLI");

            state->result = left_operand << (right_operand_signed & 0b11111);

            LOG_F(
                3,
                "%d << %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand_signed & 0b11111),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_IMM_SRLI_SRAI: {
            if ((right_operand_signed >> 10) & 0b1) {
                LOG_F(
                    3,
                    "OP IMM SRAI");

                uint32_t bitmask = 0xFFFFFFFF;
                bitmask = bitmask >> (right_operand_signed & 0b11111);
                state->result = left_operand >> (right_operand_signed & 0b11111);
                if ((left_operand >> 31) & 0b1) {
                    state->result = state->result | ~bitmask;
                }

                LOG_F(
                    3,
                    "%d >>> %d = %d",
                    static_cast<int32_t>(left_operand),
                    static_cast<int32_t>(right_operand_signed),
                    static_cast<int32_t>(state->result));
            } else {
                LOG_F(
                    3,
                    "OP IMM SRLI");

                state->result = left_operand >> (right_operand_signed & 0b11111);

                LOG_F(
                    3,
                    "%d >> %d = %d",
                    static_cast<int32_t>(left_operand),
                    static_cast<int32_t>(right_operand_signed & 0b11111),
                    static_cast<int32_t>(state->result));
            }
            break;
        }
        default: {
            LOG_F(
                FATAL,
                "UNKNOWN OP IMM FUNCT3 VALUE");
            break;
        }
    }
}

/**
 * @brief Execute load upper immediate
 */
void cpu_instruction_execute::execute_lui(
    cpu_state* state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    state->result = state->unsigned_immediate;

    LOG_F(
        3,
        "REG: %d = %d",
        static_cast<uint32_t>(state->dest_register_num),
        state->unsigned_immediate);
}

/**
 * @brief Execute an add upper immediate to program counter instruction
 */
void cpu_instruction_execute::execute_auipc(
    cpu_state* state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    state->result = this->reg_file->program_counter + state->signed_immediate;

    LOG_F(
        3,
        "PROG COUNTER = %d",
        static_cast<uint32_t>(state->result));
}

/**
 * @brief Execute a store instruction
 */
void cpu_instruction_execute::execute_store(
    cpu_state *state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    int32_t src = state->right_operand_register_value;
    int32_t base = state->left_operand_register_value;
    int32_t offset = state->signed_immediate;

    state->memory_addr = base + offset;

    switch (state->funct3) {
        case cpu_state::FUNCT3_STORE_SB: {
            LOG_F(
                3,
                "STORE BYTE");

            state->result = src & 0xFF;
            break;
        }
        case cpu_state::FUNCT3_STORE_SH: {
            LOG_F(
                3,
                "STORE HALF WORD");

            state->result = src & 0xFFFF;
            break;
        }
        case cpu_state::FUNCT3_STORE_SW: {
            LOG_F(
                3,
                "STORE WORD");

            state->result = src & 0xFFFFFFFFF;
            break;
        }
        default: {
            LOG_F(
                FATAL,
                "UNKNOWN STORE FUNCT3 VALUE");
            break;
        }
    }

    LOG_F(
        3,
        "MEM AT ADDR: %d = %d",
        state->memory_addr,
        state->result);
}

/**
 * @brief Execute an operate instruction
 */
void cpu_instruction_execute::execute_op(
    cpu_state* state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    int32_t left_operand = state->left_operand_register_value;
    int32_t right_operand = state->right_operand_register_value;

    switch (state->funct3) {
        case cpu_state::FUNCT3_OP_ADD_SUB: {
            if (state->funct7 == 0b0000000) {
                LOG_F(
                    3,
                    "OP ADD");

                state->result = left_operand + right_operand;

                LOG_F(
                    3,
                    "%d + %d = %d",
                    static_cast<int32_t>(left_operand),
                    static_cast<int32_t>(right_operand),
                    static_cast<int32_t>(state->result));
            } else if (state->funct7 == 0b0100000) {
                LOG_F(
                    3,
                    "OP SUB");

                state->result = left_operand - right_operand;

                LOG_F(
                    3,
                    "%d - %d = %d",
                    static_cast<int32_t>(left_operand),
                    static_cast<int32_t>(right_operand),
                    static_cast<int32_t>(state->result));
            } else {
                LOG_F(
                    FATAL,
                    "UNKNOWN OP ADD/SUB FUNCT7 VALUE");
            }
            break;
        }
        case cpu_state::FUNCT3_OP_SLL: {
            LOG_F(
                3,
                "OP SLL");

            state->result = left_operand << right_operand;

            LOG_F(
                3,
                "%d << %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_SRL_SRA: {
            if (state->funct7 == 0b0100000) {
                LOG_F(
                    3,
                    "OP SRA");

                uint32_t bitmask = 0xFFFFFFFF;
                bitmask = bitmask >> right_operand;
                state->result = left_operand >> right_operand;
                if ((left_operand >> 31) & 0b1) {
                    state->result = state->result | ~bitmask;
                }

                LOG_F(
                    3,
                    "%d >>> %d = %d",
                    static_cast<int32_t>(left_operand),
                    static_cast<int32_t>(right_operand),
                    static_cast<int32_t>(state->result));
            } else {
                LOG_F(
                    3,
                    "OP SRL");

                state->result = left_operand >> right_operand;

                LOG_F(
                    3,
                    "%d >> %d = %d",
                    static_cast<int32_t>(left_operand),
                    static_cast<int32_t>(right_operand),
                    static_cast<int32_t>(state->result));
            }
            break;
        }
        case cpu_state::FUNCT3_OP_SLT: {
            LOG_F(
                3,
                "OP SLT");

            state->result = left_operand < right_operand;

            LOG_F(
                3,
                "%d < %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_SLTU: {
            LOG_F(
                3,
                "OP SLTU");

            state->result = ((uint32_t) left_operand) < ((uint32_t) right_operand);

            LOG_F(
                3,
                "%d <U %d = %d",
                static_cast<uint32_t>(left_operand),
                static_cast<uint32_t>(right_operand),
                static_cast<uint32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_XOR: {
            LOG_F(
                3,
                "OP XOR");

            state->result = left_operand ^ right_operand;

            LOG_F(
                3,
                "%d XOR %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_OR: {
            LOG_F(
                3,
                "OP OR");

            state->result = left_operand | right_operand;

            LOG_F(
                3,
                "%d OR %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_OP_AND: {
            LOG_F(
                3,
                "OP AND");

            state->result = left_operand & right_operand;

            LOG_F(
                3,
                "%d AND %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        default: {
            LOG_F(
                FATAL,
                "UNKNOWN OP FUNCT3 VALUE");
            break;
        }
    }
}

/**
 * @brief Execute a branch instruction
 */
void cpu_instruction_execute::execute_branch(
    cpu_state* state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    int32_t left_operand = state->left_operand_register_value;
    int32_t right_operand = state->right_operand_register_value;

    switch (state->funct3) {
        case cpu_state::FUNCT3_BRANCH_BEQ: {
            LOG_F(
                3,
                "BRANCH BEQ");

            state->result = left_operand == right_operand;

            LOG_F(
                3,
                "%d == %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_BRANCH_BNE: {
            LOG_F(
                3,
                "BRANCH BNE");

            state->result = left_operand != right_operand;

            LOG_F(
                3,
                "%d != %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_BRANCH_BLT: {
            LOG_F(
                3,
                "BRANCH BLT");

            state->result = left_operand < right_operand;

            LOG_F(
                3,
                "%d < %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_BRANCH_BGE: {
            LOG_F(
                3,
                "BRANCH BGE");

            state->result = left_operand >= right_operand;

            LOG_F(
                3,
                "%d >= %d = %d",
                static_cast<int32_t>(left_operand),
                static_cast<int32_t>(right_operand),
                static_cast<int32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_BRANCH_BLTU: {
            LOG_F(
                3,
                "BRANCH BLTU");

            state->result = ((uint32_t) left_operand) < ((uint32_t) right_operand);

            LOG_F(
                3,
                "%d <U %d = %d",
                static_cast<uint32_t>(left_operand),
                static_cast<uint32_t>(right_operand),
                static_cast<uint32_t>(state->result));
            break;
        }
        case cpu_state::FUNCT3_BRANCH_BGEU: {
            LOG_F(
                3,
                "BRANCH BGEU");

            state->result = ((uint32_t) left_operand) >= ((uint32_t) right_operand);

            LOG_F(
                3,
                "%d >=U %d = %d",
                static_cast<uint32_t>(left_operand),
                static_cast<uint32_t>(right_operand),
                static_cast<uint32_t>(state->result));
            break;
        }
        default: {
            LOG_F(
                FATAL,
                "UNKNOWN BRANCH FUNCT3 VALUE");
            break;
        }
    }
}

/**
 * @brief Execute a jump and link instruction
 */
void cpu_instruction_execute::execute_jal(
    cpu_state* state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    state->result = this->reg_file->program_counter + 4;
    state->jump_addr = this->reg_file->program_counter + state->signed_immediate;

    LOG_F(
        3,
        "JUMP ADDR = %d",
        static_cast<uint32_t>(state->jump_addr));
}

/**
 * @brief Execute a jump and link register instruction
 */
void cpu_instruction_execute::execute_jalr(
    cpu_state* state  //!< Pointer to a cpu_state object that contains the state after instruction decode
) {
    LOG_SCOPE_FUNCTION(3);

    int32_t left_operand = state->left_operand_register_value;

    state->result = this->reg_file->program_counter + 4;
    state->jump_addr = ((state->signed_immediate + left_operand) & 0xFFFFFFFE);

    LOG_F(
        3,
        "JUMP ADDR = %d",
        static_cast<uint32_t>(state->jump_addr));
}
