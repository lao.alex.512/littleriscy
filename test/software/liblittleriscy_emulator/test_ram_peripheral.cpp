/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file test/software/liblittleriscy_emulator/test_ram_peripheral.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2021
 *
 * @brief Test the RAM peripheral
 */

#include "gtest/gtest.h"

#include "ram_peripheral.h"

TEST(test_ram_peripheral, trivial_read_write) {
    ram_peripheral test_ram(1);

    uint32_t check_value = 0xAAAA;

    test_ram.write(0, check_value);
    EXPECT_EQ(test_ram.read(0), check_value);
}
