/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file include/ram_peripheral.h
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief Header for ram_peripheral.cpp
 */
#ifndef SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_RAM_PERIPHERAL_H_
#define SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_RAM_PERIPHERAL_H_

#include <cstdint>
#include <iostream>

#include "peripheral.h"

class ram_peripheral : public peripheral{
 public:
    explicit ram_peripheral(const uint32_t num_words);
    uint32_t read(uint32_t address);
    void write(uint32_t address, uint32_t data);
 private:
    uint32_t* mem = NULL;
};

#endif  // SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_RAM_PERIPHERAL_H_
