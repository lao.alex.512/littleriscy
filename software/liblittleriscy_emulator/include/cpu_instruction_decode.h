/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file include/cpu_instruction_decode.h
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief Header for cpu_instruction_decode.cpp
 */

#ifndef SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_INSTRUCTION_DECODE_H_
#define SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_INSTRUCTION_DECODE_H_

#include <cstdint>

#include "cpu_state.h"

class cpu_instruction_decode {
 public:
    cpu_instruction_decode();
    cpu_state decode_instruction(uint32_t instruction);
 private:
    cpu_state decode_load(uint32_t instruction);
    cpu_state decode_op_imm(uint32_t instruction);
    cpu_state decode_auipc(uint32_t instruction);
    cpu_state decode_store(uint32_t instruction);
    cpu_state decode_op(uint32_t instruction);
    cpu_state decode_lui(uint32_t instruction);
    cpu_state decode_branch(uint32_t instruction);
    cpu_state decode_jalr(uint32_t instruction);
    cpu_state decode_jal(uint32_t instruction);
    cpu_state decode_system(uint32_t instruction);
};


#endif  // SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_INSTRUCTION_DECODE_H_
