# Build liblittleriscy_emulator Library

# Add includes
include_directories(./include/)

# Add sources
add_subdirectory(./src/)
